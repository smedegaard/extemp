defmodule Extemp.Temperature do
  use GenServer
  require Logger

  @base_dir "/sys/bus/w1/devices/"
  @interval 2000

  def start_link(opts) do
    GenServer.start_link(__MODULE__, opts, name: __MODULE__)
  end

  def init(_opts) do
    sensors = get_sensors()
    send(self(), :read_temp)
    {:ok, %{
        sensors: sensors,
        temperatures: %{}
    }}
  end

  def handle_info(:read_temp, state) do
    temps =
      state[:sensors]
      |> Enum.with_index
      |> Enum.map(&read_temp(&1, @base_dir))
      |> IO.inspect

    state = %{state | temperatures: temps}
    Process.send_after(self(), :read_temp, @interval)
    {:noreply, state}
  end

  defp get_sensors do
    File.ls!(@base_dir)
    |> Enum.filter(&(String.starts_with?(&1, "28-")))
  end

  defp read_temp({sensor, index}, base_dir) do
    sensor_data = File.read!("#{base_dir}#{sensor}/w1_slave")
    # Logger.debug("reading sensor: #{sensor}: #{sensor_data}")
    {temp, _} = Regex.run(~r/t=(\d+)/, sensor_data)
    |> List.last
    |> Float.parse

    atom =
      "sensor_" <> Integer.to_string(index)
      |> String.to_atom

    %{atom => temp / 1000}
  end

end
