defmodule Extemp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  require RingLogger
  require Logger

  @target Mix.Project.config()[:target]

  use Application

  def start(_type, _args) do
    RingLogger.attach

    Logger.debug "### START ###"

    opts = [strategy: :one_for_one, name: Extemp.Supervisor]
    Supervisor.start_link(children(@target), opts)
  end

  # List all child processes to be supervised
  def children("host") do
    [
      # Starts a worker by calling: Extemp.Worker.start_link(arg)
      # {Extemp.Worker, arg},
    ]
  end

  def children(_target) do
    [
      # Starts a worker by calling: Extemp.Worker.start_link(arg)
      # {Extemp.Worker, arg},
      {Extemp.Temperature, []}
    ]
  end
end
 
